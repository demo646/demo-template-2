import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

import { reduxCommonActions, reduxMappingActions, reduxMappingReducer, reduxParsePagination } from '../../utils/CoreUtils';

/* ------------- Initial State ------------- */

const INITIAL_STATE = Immutable({
  error: {},
  fetching: {},
  content: {},
  pagination: {},
});

const actions = {
  ...reduxCommonActions('evaluation'),
  // CRUD
  crudEvaluationRequest: [['classify', 'payload'], (state, { classify, payload }) => {
    const { type } = payload;
    let fetch = true;

    if (type === "get" || type === "delete") fetch = true;
    if (type === "add" || type === "update") fetch = false;

    return state.merge({
      fetching: { ...state.fetching, [classify]: fetch }
    })
  }],
  crudEvaluationSuccess: [['classify', 'payload'], (state, { classify, payload }) => {
    const { type, data } = payload;
    let contents;
    let paginations;

    switch (type) {
      case 'get':
        contents = data.items;
        paginations = reduxParsePagination(data)
        return state.merge({
          fetching: { ...state.fetching, [classify]: false },
          content: { ...state.content, [classify]: contents },
          pagination: { ...state.pagination, [classify]: paginations }
        })
      case 'add':
        contents = [data, ...state.content[classify]];
        break;
      case 'update':
        contents = [...state.content[classify]].map(item => item._id === data._id ? data : item);
        break;
      case 'delete':
        contents = [...state.content[classify]].filter(item => item._id !== data._id);
        break;
      default:
        break;
    }

    return state.merge({
      fetching: { ...state.fetching, [classify]: false },
      content: { ...state.content, [classify]: contents },
    })
  }]
};

/* ------------- Create Actions And Hookup Reducers To Types ------------- */

const { Types, Creators } = createActions(reduxMappingActions(actions));
const reducer = createReducer(INITIAL_STATE, reduxMappingReducer(actions, Types));

export { Types, reducer };
export default Creators;
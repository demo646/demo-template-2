import React, { useState, useLayoutEffect } from 'react';

// utils
import { getRouterQuery } from '../../../../../utils/RouterUtils';

// mui
import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '100%',
    height: '100%',

    '&.MuiPaper-elevation1': {
      boxShadow: '0px 4px 16px 0px rgb(125 125 125 / 8%)',
    },
  },
}));

function CEListSkeleton() {
  const classes = useStyles();
  const [count, setCount] = useState(5);
  const { size } = getRouterQuery();

  useLayoutEffect(() => {
    if (size) {
      setCount(+size);
    }
  }, [size]);

  return (
    <>
      {Array.from(new Array(count)).map((item, index) => (
        <Grid item key={index} xs={12} sm={6} md={4}>
          <Card className={classes.root}>
            <Skeleton variant="rect" animation="wave" height={120} width="100%" />
          </Card>
        </Grid>
      ))}
    </>
  );
}

export default CEListSkeleton;

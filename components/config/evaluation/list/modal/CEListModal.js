import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

// action
import EvaluationRedux from '../../../../../redux/evaluation/EvaluationRedux';

// utils
import { getRouterQuery } from '../../../../../utils/RouterUtils';

// mui
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

function CEListModal({ open, handleClose, classify, onEdit, clearEdit, clearPaginate }) {
  const [name, setName] = useState('');
  const [scopes, setScopes] = useState(['call']);
  const dispatch = useDispatch();
  const { page } = getRouterQuery();

  useEffect(() => {
    if (onEdit) {
      setName(onEdit?.name);
      setScopes(onEdit?.scopes);
    } else {
      setName('');
      setScopes(['call']);
    }
  }, [onEdit]);

  const handleChange = (e) => {
    const { value } = e.target;

    setScopes((prevScopes) => {
      const checked = scopes.includes(value);
      if (checked) {
        // uncheck
        return scopes.filter((item) => item !== value);
      } else {
        // check
        return [...prevScopes, value];
      }
    });
  };

  const handleAddEvaluation = () => {
    // simple validate
    if (name.length < 1) {
      return;
    } else if (scopes.length <= 0) {
      return;
    }

    const data = {
      name,
      scopes,
      contact_ids: [],
      contact_category_ids: [],
    };

    if (onEdit) {
      dispatch(EvaluationRedux.crudEvaluationRequest(classify.evaluation, { type: 'update', data: { id: onEdit.id, body: data } }))
    } else {
      dispatch(EvaluationRedux.crudEvaluationRequest(classify.evaluation, { type: 'add', data }))
      if (Number(page) > 1) {
        clearPaginate();
      }
    }

    handleClose();
    setName('');
    setScopes('call');
  };

  const handleCloseModal = () => {
    handleClose();
    clearEdit();
  };

  return (
    <div>
      <Dialog open={open} onClose={handleCloseModal} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          {onEdit ? 'Cập nhật khách hàng' : 'Thêm khách hàng'}
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            value={name}
            onChange={(e) => setName(e.target.value)}
            id="name"
            label="Tên khách"
            type="name"
            fullWidth
            autoComplete="off"
          />
          <FormGroup row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={scopes.includes('call')}
                  name="call"
                  value="call"
                  onChange={handleChange}
                />
              }
              label="Đánh giá lịch sử gọi"
            />
            <FormControlLabel
              control={
                <Checkbox
                  checked={scopes.includes('contact')}
                  name="contact"
                  value="contact"
                  onChange={handleChange}
                />
              }
              label="Đánh giá khách hàng"
            />
          </FormGroup>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseModal} color="primary">
            Hủy
          </Button>
          <Button onClick={handleAddEvaluation} color="primary">
            {onEdit ? 'Cập nhật' : 'Thêm'}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default CEListModal;

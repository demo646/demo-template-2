import React, { useState, useEffect, useImperativeHandle } from 'react';
import { useDispatch } from 'react-redux';

// actions
import EvaluationActions from '../../../../../redux/evaluation/EvaluationRedux';

// utils
import { replaceRoute, getRouterQuery } from '../../../../../utils/RouterUtils';

// mui
import makeStyles from '@material-ui/core/styles/makeStyles';
import TablePagination from '@material-ui/core/TablePagination';
import Pagination from '@material-ui/lab/Pagination';
import Skeleton from "@material-ui/lab/Skeleton"

const useStyles = makeStyles((theme) => ({
  paginations: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center',
    },

    '& > *': {
      marginTop: theme.spacing(2),
    },
    '& .MuiTablePagination-actions': {
      display: 'none',
    },
    // '& .MuiTablePagination-input': {
    //   display: 'none',
    // },
  },
}));

const replaceUrl = (params) =>
  replaceRoute(`/config/evaluation/list?page=${params.value}&size=${params.size}`);

const CEListPagination = React.forwardRef((props, ref) => {
  const { count, totalCount, classify } = props;
  const classes = useStyles();
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(5);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(EvaluationActions.crudEvaluationRequest(classify.evaluation, { type: 'get', params: { page, size } }))
  }, [page, size]);

  useEffect(() => {
    const { page: searchPage, size: searchSize } = getRouterQuery();
    if (searchPage, searchSize) {
      setPage(+searchPage);
      setSize(+searchSize);
    }
  }, [])

  const handlePageChange = (event, value) => {
    setPage(value);
    replaceUrl({ value, size });
  };

  const handleChangeRowsPerPage = (event) => {
    setSize(parseInt(event.target.value));
    setPage(1)
    replaceUrl({ value: page, size: parseInt(event.target.value) });
  };

  useImperativeHandle(ref, () => ({
    clearPagination() {
      clearPagination()
    }
  }))

  const clearPagination = () => {
    setPage(1);
    setSize(5);
    replaceUrl({ value: 1, size: 5 });
  };

  return (
    <div className={classes.paginations}>
      {totalCount <= 0 ? <Skeleton variant="text" height={30} width="45%" /> : <TablePagination
        labelRowsPerPage={`Số card mỗi trang`}
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={totalCount}
        page={page - 1}
        onPageChange={handlePageChange}
        rowsPerPage={size}
        onRowsPerPageChange={handleChangeRowsPerPage}
        labelDisplayedRows={({ from, to, count }) =>
          `${from}-${to} của ${count !== -1 ? count : `lớn hơn ${to}`}`
        }
      />}
      {count ? <Pagination onChange={handlePageChange} color="primary" count={count} page={page} /> :
        <Skeleton variant="text" height={30} width="55%" />
      }
    </div>
  );
})

export default CEListPagination;

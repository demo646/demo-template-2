import React from 'react';
import { useDispatch } from 'react-redux';

// actions
import EvaluationRedux from '../../../../../redux/evaluation/EvaluationRedux';

// mui
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import CardActionArea from '@material-ui/core/CardActionArea';

// componnets
import { CEListConfirmModal } from '../modal';

// icon
import trashIcon from '../../../../../assets/icons/common/ic_trash_can_r.png';

const useStyles = makeStyles({
  root: {
    minWidth: '100%',
    height: '100%',
    position: 'relative',
    fontSize: '0.875rem',

    '&:hover img': {
      display: 'block',
    },
    '&.MuiPaper-elevation1': {
      boxShadow: '0px 4px 16px 0px rgb(125 125 125 / 8%)',
    },
  },
  title: {
    fontSize: '0.875rem',
    fontWeight: '600',
  },
  pos: {
    marginBottom: 12,
    fontSize: '0.875rem',
  },
  icon: {
    width: 20,
    height: 20,
    position: 'absolute',
    top: 10,
    right: 10,
    cursor: 'pointer',
    display: 'none',
    zIndex: 99999,
  },
  cardChip: {
    fontSize: '0.875rem',
    borderRadius: '7px',
    height: '25px',
    margin: '10px 7px 0px 0px',

    "&:nth-child(2)": {
      marginLeft: '0px',
    }
  },
});

const chips = [
  { key: 'call', value: 'Lịch sử cuộc gọi' },
  { key: 'contact', value: 'Khách hàng' },
];

export default function CEListCard({
  name,
  scopes,
  id,
  classify,
  handleOpen,
  handleEdit,
  clearPaginate,
}) {
  const classes = useStyles();
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const dispatch = useDispatch();

  const handleClick = () => {
    setOpenConfirm(true);
  };

  const handleUpdate = () => {
    const data = {
      id,
      name,
      scopes,
    };
    handleOpen();
    handleEdit(data);
  };

  const handleDelete = () => {
    dispatch(EvaluationRedux.crudEvaluationRequest(classify.evaluation, { type: 'delete', data: id }));
    setOpenConfirm(false);
    clearPaginate();
  };

  return (
    <Card className={classes.root}>
      <CEListConfirmModal open={openConfirm} setOpen={setOpenConfirm} handleDelete={handleDelete} />
      <img onClick={handleClick} className={classes.icon} src={trashIcon} alt="trash" />
      <CardActionArea onClick={handleUpdate}>
        <CardContent>
          <Typography className={classes.title} gutterBottom>
            {name}
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            Chưa có phân quyền
          </Typography>
          {chips.map((item, index) => {
            return scopes?.includes(item.key) ? (
              <Chip
                key={item.key + item.value + index}
                className={classes.cardChip}
                label={item.value}
              />
            ) : (
              ''
            );
          })}
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

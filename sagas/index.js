import { all } from 'redux-saga/effects';

/* ------------- Sagas ------------- */

import UserSagas from './user';
import EvaluationsSagas from "./evaluation"

/* ------------- Connect all Sagas to root ------------- */

export default function* rootSaga() {
  yield all([
    ...UserSagas,
    ...EvaluationsSagas,
  ]);
}
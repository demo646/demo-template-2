import { sagasMappingEffects } from '../../utils/CoreUtils';

import { Types } from '../../redux/evaluation/EvaluationRedux';
import * as Sagas from './EvaluationSaga';

const keys = [
  ['crudEvaluation']
];

/* ------------- Connect Types To Sagas ------------- */

export default sagasMappingEffects(keys, Types, Sagas);
import { call, put } from 'redux-saga/effects';

import EvaluationActions from '../../redux/evaluation/EvaluationRedux';

import EvaluationsAPIs from '../../services/APIs/evaluation/EvaluationAPIs';

import { validateResp, getErrorMsg } from '../../utils/StringUtils';

export function* crudEvaluation(action) {
  const { classify, payload } = action;
  const { type } = payload;
  try {
    let resp = yield call(EvaluationsAPIs.crudEvaluation, type, payload)
    if (validateResp(resp)) {
      console.log('running', type)
      yield put(EvaluationActions.crudEvaluationSuccess(classify, { type, data: resp.payload }));
    } else throw resp;
  } catch (error) {
    yield put(EvaluationActions.evaluationCommonFailure(classify, getErrorMsg(error)));
  }
}


import { doRequest } from '../../../utils/CoreUtils';

import { domains } from '../../../constants';

export default Object.freeze({
    userLogin: (body) => {
        let url = `${domains.auth}auth/pre_auth`;
        return doRequest('post', url, { body });
    },
    getLoginToken: (body) => {
        let url = `${domains.auth}auth/login`;
        return doRequest('post', url, { body });
    },
    getUserInfo: ({ id } = {}) => {
        let url = `${domains.contact}contact/agent/get/${id}`;
        return doRequest('get', url);
    },
});
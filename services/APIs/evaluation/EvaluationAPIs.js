import { doRequest } from '../../../utils/CoreUtils';

import { domains } from '../../../constants';

export default Object.freeze({
  crudEvaluation: (type, payload) => {
    let url = `${domains.contact}/evaluation_criteria/`;
    switch (type) {
      case 'get':
        const { params } = payload;
        url = `${url}search?page=${params?.page}&size=${params?.size}`;
        return doRequest('get', url)
      case 'add':
        url = `${url}add`;
        return doRequest('post', url, { body: payload?.data });
      case 'update':
        const { id, body } = payload?.data;
        url = `${url}update/${id}`;
        return doRequest('put', url, { body });
      case 'delete':
        url = `${url}delete/${payload?.data}`;
        return doRequest('delete', url);
    }
  }
});

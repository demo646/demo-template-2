
export const styles = (theme) => ({
  wrapper: {
    width: '100%',
    height: '100%',
    overflow: 'auto',
  },
  container: {
    marginTop: '3rem',
    height: 'fit-content',
    overflow: 'hidden',
    maxWidth: 'calc(100% - 96px)',
    padding: '32px 48px',
    margin: 46,

    [theme.breakpoints.down('lg')]: {
      maxWidth: 'calc(100% - 60px)',
      margin: 30,
      padding: '22px 38px',
    },

    [theme.breakpoints.down('sm')]: {
      maxWidth: 'calc(100% - 40px)',
      margin: 20,
      padding: '12px 28px',
    },
    [theme.breakpoints.down('xs')]: {
      maxWidth: 'calc(100% - 20px)',
      margin: 10,
      padding: '0px 0px',
    },
  },
  button: {
    margin: '15px 15px 15px 0px'
  },
  paginations: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center',
    },

    '& > *': {
      marginTop: theme.spacing(2),
    },
    '& .MuiTablePagination-actions': {
      display: 'none',
    },
    // '& .MuiTablePagination-input': {
    //   display: 'none',
    // },
  },
});

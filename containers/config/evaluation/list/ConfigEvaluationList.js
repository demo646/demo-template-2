import React, { createRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

// Actions
import EvaluationActions from '../../../../redux/evaluation/EvaluationRedux';

// utils
import { pushRoute } from '../../../../utils/RouterUtils';

// styles
import { withStyles } from '@material-ui/core/styles';
import { styles } from './styles';

// mui
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

// components
import { CEListModal } from '../../../../components/config/evaluation/list/modal';
import CEListCard from '../../../../components/config/evaluation/list/card';
import CEListSkeleton from "../../../../components/config/evaluation/list/skeleton"
import { NotData } from '../../../../components/common';
import CEListPagination from '../../../../components/config/evaluation/list/pagination';

class ConfigEvaluationList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      classify: {
        evaluation: 'evaluations',
      },
      openModal: false,
      onEdit: false,
    };
    this.myRef = createRef()
  }

  handleClose = () => {
    this.setState({ openModal: false });
  };

  handleOpen = () => {
    this.setState({ openModal: true });
  };

  handleEdit = (data) => {
    this.setState({ onEdit: data });
  };

  clearEdit = () => {
    this.setState({ onEdit: false });
  };

  clearPaginate = () => {
    this.myRef.current.clearPagination()
  };

  _renderNoData = () => {
    const { classify } = this.state;
    const { error, classes } = this.props;
    return (
      <>
        <Button
          className={classes.button}
          onClick={() => pushRoute('/welcome')}
          color="secondary"
        >
          Go to homepage
        </Button>
        <NotData message={error[classify.evaluation]} />
      </>
    )
  }

  render() {
    const { classify, openModal } = this.state;
    const { classes, fetching, content, pagination, error } = this.props;

    if (error[classify.evaluation]) return this._renderNoData();

    const totalCount = pagination[classify.evaluation]
      ? pagination[classify.evaluation]?.total_items
      : 0;

    return (
      <div className={classes.wrapper}>
        <div className={classes.container}>
          <CEListModal
            classify={classify}
            open={openModal}
            handleClose={this.handleClose}
            onEdit={this.state.onEdit}
            clearEdit={this.clearEdit}
            clearPaginate={this.clearPaginate}
          />
          <Button
            className={classes.button}
            onClick={this.handleOpen}
            color="primary"
            variant="outlined"
          >
            Thêm
          </Button>
          <Button
            className={classes.button}
            onClick={() => pushRoute('/welcome')}
            color="secondary"
          >
            Trở về
          </Button>
          <div style={{ minHeight: '70vh' }}>
            <Grid style={{ overflow: 'hidden' }} container spacing={3}>
              {(fetching[classify.evaluation]
                ? <CEListSkeleton />
                : content[classify.evaluation]?.map((item, index) => (
                  <Grid item key={item._id} xs={12} sm={6} md={4}>
                    <CEListCard
                      name={item.name}
                      scopes={item.scopes}
                      id={item._id}
                      classify={classify}
                      handleOpen={this.handleOpen}
                      handleEdit={this.handleEdit}
                      clearPaginate={this.clearPaginate}

                    />
                  </Grid>
                )
                ))}
            </Grid>
          </div>
          <CEListPagination ref={this.myRef} classify={classify} count={pagination[classify.evaluation]?.total_pages} totalCount={totalCount} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => state.evaluation;

const mapDispatchToProps = (dispatch) => ({
  crudEvaluationRequest: (classify, payload) => dispatch(EvaluationActions.crudEvaluationRequest(classify, payload)),
});

export default compose(withStyles(styles), connect(mapStateToProps, mapDispatchToProps))(ConfigEvaluationList);